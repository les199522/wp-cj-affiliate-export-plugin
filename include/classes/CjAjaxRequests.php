<?php 
/**
*
*
*
* 
*/

class AjaxRequests {
	
	function __construct() {
		self::addEvents();
	}

	public static function addEvents() {
		$events = array(
			'cj_run_export'
		);

		foreach ( $events as $event ) {
			add_action( 'wp_ajax_'.$event, array( __CLASS__, $event ) );
		}
	}

	public static function cj_run_export() {
		check_ajax_referer( 'cj-export-nonce', 'nonce' );
		
		$dataQuery = $_POST['query_data'];
		$CjExportObject = new CjExport( $dataQuery['file_type'], $dataQuery['exportfile'], $_POST['offset'] );

	// Only in last offset step
		if ( $CjExportObject->getStatus( $_POST['laststep'], $_POST['offset'] ) == 'true' ) {
			switch ( $dataQuery['transfer_options'] ) {
				case 'ftp':
					$result = CJAffiliate_plugin::sendfile_ftp( $CjExportObject->getFilepath() );
					break;
				case 'email':
					$result = CJAffiliate_plugin::sendfile_email( $CjExportObject->getFilepath(), $dataQuery['send_to_email'] );
					break;
				case 'download':
					$result  = CJAffiliate_plugin::sendfile_download( $CjExportObject->getFilepath() );
					break;
				default:
					$result = false;
					break;
			}
 			if ( isset( $result ) && !empty('type') )
				wp_send_json_success( $result );
		}

		wp_send_json_error();
	}


}	

new AjaxRequests;