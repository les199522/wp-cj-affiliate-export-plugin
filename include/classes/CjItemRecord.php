<?php 




/**
* 
*/
class CjItemRecord {

	/**
	 * Product Id
	 * @var int
	 */
	protected $ID;

	/**
	 * Array for export
	 * @var array
	 */
	protected $data;
	protected $variations;



	function __construct( $id, $currency, $wc_factory ) {
		$this->ID = $id;
		$this->create_data_array( $currency, $wc_factory );
		$this->create_item_variations( $currency, $wc_factory );
	}

	public function get_productObject() {
		if ( empty( $this->data ) )
			return false;

		$resultsArray = $this->data;

		foreach ( $resultsArray as $key => $value ) {
			switch ( $key ) {
				case 'name':
					$resultsArray[$key] = $this->substr_xml( $value, 160, true );
					break;
				case 'description':
					$resultsArray[$key] = $this->substr_xml( $value, 3000, true );
					break;
				case 'keywords':
					$resultsArray[$key] = json_encode( $value );
					break;
				default:
					break;
			}
		}

		return $resultsArray;
	}

	public function get_variationsObject( $item ) {
		foreach ( $item as $key => $value ) {
			switch ( $key ) {
				case 'name':
					$item[$key] = $this->substr_xml( $value, 160, true );
					break;
				case 'description':
					$item[$key] = $this->substr_xml( $value, 3000, true );
					break;
				case 'keywords':
					if ( in_array('additionalName', $value) )
						unset($value['additionalName']);

					$item[$key] = json_encode( $value );
					break;
				default:
					break;
			}
		}

		return $item;
	}

	public function get_variationsItems() {
		if ( !empty( $this->variations ) ) {
			return $this->variations;
		}
		return false;
	}

	public function get_csv_object() {
		return false;
	}


	protected function create_data_array( $currency, $_wc ) {
		$item = array();
		$additional_keywords = array();
		$productObject = $_wc->get_product( $this->ID );
		if ( !isset( $productObject ) )
			return false;

		$name = wp_get_post_terms( $this->ID, 'artists', array('fields'=>'names') );
		if ( isset( $name[0] ) ) {
			$productObject_variable = new WC_Product_Variable( $this->ID );
			
			$desc = $productObject->get_description();
			$stock = $productObject->is_in_stock() ? 'Yes' : 'No';
			$imageurl = wp_get_attachment_image_src( get_post_thumbnail_id( $this->ID ) );
			$keywords = $this->create_keywords( $name[0] );
			$price = $productObject_variable->get_variation_price('min');
		// Build item data.
			$item = array(
				'name'			=> (isset($keywords['additionalName'])) ? $keywords['additionalName'] : $name[0],
				'keywords'		=> ($keywords != false) ? $keywords : '',
				'description'	=> ( !empty( $desc ) ) ? $desc : 'Description is missing',
				'sku'			=> empty( $productObject->get_sku() ) ? $this->ID : $productObject->get_sku(),
				'buyurl'		=> htmlspecialchars( $productObject->get_permalink() ),
				'price'			=> ( !empty($price) ) ? $price : 0,
				'currency'		=> $currency,
				'instock'		=> $stock,
				'available'		=> 'Yes' 
			);

			if ( isset($imageurl) )
				$item['imageurl'] = $imageurl[0];

			if ( !empty( $item ) )
				$this->data = $item;
		}
		return false;
	}

	protected function create_item_variations( $currency, $_wc  ) {
		$productObject = $_wc->get_product( $this->ID );
		$mainName = $this->get_item_name();
		$keywords = $this->get_item_keywords();
		$buyurl = $this->get_item_buyurl();

		if ( !$productObject->is_type('variable') || empty($mainName) )
			return false;

		$productObject_variable = new WC_Product_Variable( $this->ID );
		$variables = $productObject_variable->get_available_variations();

		if ( !empty( $variables ) ) {
			foreach ( $variables as $item ) {
				if ( !empty( $item['display_price'] ) ) {
					if ( isset( $item['attributes']['attribute_pa_seat-location'] ) ) {
						$keywords['ticket_type'] = $item['attributes']['attribute_pa_seat-location'];
					}
					$stock = ($item['is_in_stock'] == 1) ? 'Yes' : 'No';

					$this->variations[] = array(
						'name'			=> $mainName,
						'keywords'		=> $keywords,
						'description'	=> ( !empty( $item['variation_description'] ) ) ? $item['variation_description'] : 'Description is missing',
						'sku'			=> $this->ID . '-' . $item['variation_id'],
						'buyurl'		=> $buyurl,
						'price'			=> $item['display_price'],
						'currency'		=> $currency,
						'instock'		=> $stock,
						'available'		=> 'Yes' 
					);
				}
			}
		}
	}


	private function create_keywords( $name = '' ) {
		$post_id = $this->ID;
		$keywords_array = array();

		$city = get_post_meta( $post_id, 'City', true );
		if ( isset( $city ) && !empty( $city ) )
			$keywords_array['city'] = $city;

		$country = get_post_meta( $post_id, 'country', true );
		if ( isset( $country ) && !empty( $country ) )
			$keywords_array['country'] = $country;

		$venue = get_post_meta( $post_id, 'Venue', true );
		if ( isset( $venue ) && !empty( $venue ) )
			$keywords_array['stadium'] = $venue;

		$date = get_post_meta( $post_id, '_date', true );
		if ( isset( $date ) && !empty( $date ) ) {
			$keywords_array['event_date'] = str_replace('/', '-', $date);
			$keywords_array['event_date'] = $keywords_array['event_date'] . 'T20:34:00Z';
		}

		$event_type_list = wp_get_post_terms( $post_id, 'product_cat', array( 'fields' => 'names' ));
		if ( $event_type_list ) {
			$event_type = implode(">", $event_type_list);
			if ( !empty( $event_type ) ) {
				$keywords_array['event_type'] = $event_type;
			}
		// Additional parametrs for special evets
			if ( in_array('Rugby', $event_type_list) || in_array('Boxing', $event_type_list) ) {
				if ( $additionalName = $this->get_cut_title_vs() ) {
					$keywords_array['additionalName'] = $additionalName;
					$keywords_array['Category'] = $name;
				}
			}
		}

		if ( !empty( $keywords_array ) ) {
			return $keywords_array;
		}

		return false;
	}

	private function get_cut_title_vs() {
	// England vs Ireland – Twickenham Stadium – 17/03/2018
		$specialName = get_the_title( $this->ID );
	// Return --> England vs Ireland				
		$specialCutName = stristr($specialName, '&#8211;', true);
		if ( $specialCutName ) {
			return $specialCutName;
		}
		return false;
	}

	private function substr_xml( $string, $chars, $clean = false ) {
		if ( empty( $string ) )
			return false;

		$decode_string = html_entity_decode( $string );
		$string = substr( $decode_string, 0, $chars );
		if ( $clean )
			return $this->clean_string( $string );

		return $string;
	}

	private function clean_string($string) {
	    $string = preg_replace('#[^A-Za-z0-9\. -)(]+#', '', $string); // Removes special chars.
	    return preg_replace('#[\\s-]+#', ' ', $string); // Removes multiple spaces chars.
	}


	public function get_item_name() {
		return $this->data['name'];
	}

	public function get_item_buyurl() {
		return $this->data['buyurl'];
	}


	public function get_item_keywords() {
		return $this->data['keywords'];
	}

}