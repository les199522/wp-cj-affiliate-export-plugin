jQuery(document).ready(function($) {
	var queryStack = [];

	function getFilledArray( count, postInQuery )  {
		for ( var i = 0; i <= count/postInQuery; i++ ) {
			queryStack.push(i*postInQuery);
		}
		return queryStack;
	}

	$('.js-cjExport').on('submit', function(event) {
		event.preventDefault();
 		var query_data = {},
 			promise = $.when(),
 			postCount = $(this).find('.js-cjExport-btn').data('postcount');


		$.each( $(this).serializeArray() , function( index, val ) {
			query_data[val.name] = val.value;
		});

 		$('.js-cjExport-btn').prop('disabled',true);
		$('.cj-spinner').show();
		$.each( getFilledArray( postCount, 100 ), function( index, offset ){
			promise = promise.then(function(){
				return $.ajax({
					url: CJExport.ajaxurl,
					type: 'POST',
					data: {
						action: 'cj_run_export',
						nonce : CJExport.noncefield,
						offset: offset,
						laststep : queryStack[queryStack.length-1],
						query_data
					},
				});
			}).then(function(data){
				console.log(data);
				if ( data.success == true ) {
					if ( data.data.type == 'download' ) {
						$('.js-save-file').attr( 'href', data.data.returnValue );
						$('.js-save-file').addClass('show');
					}	
				}
			}).fail(function(){
					$('.js-cjExport-btn').addClass('btn-warning');
			});
		});
		promise.then(function(){
   			$('.cj-spinner ').hide();
	 		$('.js-cjExport-btn').prop('disabled',false);
			$('.js-cjExport-btn').addClass('btn-success');
		});
	});

	/** Show/Hide email field */
	$(document).on('change', '.js-transfer-select', function(event) {
		event.preventDefault();
		/* Act on the event */
		if ( $(this).val() == 'email' ) {
			$('.email-group').show();
		}else {
			$('.email-group').hide();
		}

		if ( $(this).val() == 'ftp' ) {
			validateTransfer();
		}
	});

	validateTransfer();
	function validateTransfer() {
		if ($('.js-transfer-select').val() == 'ftp' ) {
			var form = $('.js-transfer-validate');
			form.find('input[type="text"], input[type="password"]').each(function(index, el) {
				if($(el).val() == '') {
					$(el).parent('label').addClass('input-error');
				}else {
					$(el).parent('label').removeClass('input-error');
				}				
			});
		}
	}
});